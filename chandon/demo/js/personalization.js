function initMap() {
  var mapDiv = document.getElementById('map');
  var myLatLng = {lat: 50.0283953, lng: 19.9325243};
  var map = new google.maps.Map(mapDiv, {
    center: myLatLng,
    zoom: 13,
    zoomControl: true,
    streetViewControl: true,
    fullscreenControl: true
  });
  var marker = new google.maps.Marker({
     position: myLatLng,
     map: map,
     title: 'Chandon Waller & Partners'
  });
}


scrollPage = function (ev) {
  ev.preventDefault();

  var marginVal,
      $elem = $($(this).find('a').attr('href'));

  if ($(window).width() < 769) {
    marginVal = 0;
  } else {
    marginVal = 40;
  }

  $('html, body').stop().animate({
      scrollTop: $elem.offset().top - marginVal
  }, 2000);
};

$(document).ready(function() {
  $(".item").on('click', scrollPage);
});
