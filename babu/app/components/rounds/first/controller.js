// create the controller and inject Angular's $scope
angular.module('app').controller('roundFirstController', function($scope) {

    if (screenfull.enabled) {
        screenfull.request();
    }

alert(screen.height);
alert(screen.width);

    var pointsGroup = angular.element(document.getElementById('points'));
    var startBtn = document.getElementById('start_btn');
    var moviePlace = document.getElementById('movie');
    var loaderArea = document.getElementById('startArea');
    var videos = {
        "intro": document.getElementById('videoIntro'),
        "goodChoice": document.getElementById('videoGoodChoice'),
        "badChoice": document.getElementById('videoBadChoice'),
        "neutral": document.getElementById('videoNeutral')
    };
    var summaryPointsGroup = angular.element(document.getElementById('summary'));
    var videoSrc = {
        "neutral": "assets/video/1920x1080/Babu_neutral_1.mp4",
        "good": "assets/video/1920x1080/Babu_kuchnia_dobrze.mp4",
        "bad": "assets/video/1920x1080/Babu_kuchnia_zle.mp4",
        "intro": "assets/video/1920x1080/Babu_kuchnia_intro.mp4"
    };

    $scope.message = 'runda I';
    $scope.summary = 0;
    $scope.endRound = 0;
    $scope.startRound = 1;
    $scope.introEnd = 0;
    $scope.things = {
        "all": [{
            id: 1,
            properList: 0,
            improperList: 1
        }, {
            id: 2,
            properList: 0,
            improperList: 1
        }, {
            id: 3,
            properList: 0,
            improperList: 1
        }, {
            id: 4,
            properList: 1,
            improperList: 0
        }, {
            id: 5,
            properList: 0,
            improperList: 1
        }, {
            id: 6,
            properList: 1,
            improperList: 0
        }, {
            id: 7,
            properList: 1,
            improperList: 0
        }, {
            id: 8,
            properList: 1,
            improperList: 0
        }, {
            id: 9,
            properList: 1,
            improperList: 0
        }, {
            id: 10,
            properList: 0,
            improperList: 1
        }, ]
    }

    $scope.dragCallback = function(event, ui, item) {
        $scope.selected = item;
    };

    $scope.dropPlaceCallback = function(event, ui) {
        $scope.endRound += 1;
        if ($scope.selected.improperList === 1) {
            $scope.summary += $scope.selected.improperList;
            addPoint();
        }
    };

    $scope.dropBabuCallback = function(event, ui) {
        $scope.endRound += 1;
        if ($scope.selected.properList === 1) {
            $scope.summary += $scope.selected.properList;
            showVideoAfterGoodChoice();
            addPoint();
        } else {
            showVideoAfterBadChoice();
        }
        HideElementDroppedOnBabu($scope.selected.id);
    };

    function addPoint() {
        var pointImg = angular.element('<div class="point"></div>');
        pointsGroup.append(pointImg);
        var pointImg = angular.element('<div class="point"></div>');
        summaryPointsGroup.append(pointImg);
    };

    function showVideoOnRoundStart() {

        PlayVideo('intro');
        //  videos.intro.play();
        setTimeout(startRound, 4000);
    };

    function showVideoAfterGoodChoice() {
        ChangeVideoDisplayProperty('goodChoice');
        PlayVideo('goodChoice');
    };

    function showVideoAfterBadChoice() {
        ChangeVideoDisplayProperty('badChoice');
        PlayVideo('badChoice');
    };

    function showVideoNeutral() {}

    function startRound() {
      alert("start")
        videos.intro.style.display = "inline";
        videos.intro.muted = false;
        videos.intro.currentTime = 0;
        videos.intro.play();
        alert("after play")
    }

    function HideElementDroppedOnBabu(id) {
        var element = document.getElementById('thing-' + id);
        element.style.display = "none";
    }


    videos.intro.addEventListener('loadeddata', function() {
          alert('intro is ready');
    });
    videos.intro.addEventListener('seeked', function() {
    //  videos.intro.currentTime = 0;
    //  videos.intro.play();
    alert("seeked")
    if($scope.introEnd == 0){
          alert("before intro")
          ChangeVideoDisplayProperty('intro');
          alert("intro end")
          $scope.introEnd = 1;
        }
        //    videos.intro.muted = false;
    });
    videos.intro.addEventListener('loadedmetadata', function() {
          alert('intro metadata is ready');
    });
    videos.goodChoice.addEventListener('loadeddata', function() {
        //  alert('goodChoice is ready');
    });
    videos.badChoice.addEventListener('loadeddata', function() {
        //  alert('badChoice is ready');
    });
    videos.intro.addEventListener('ended', function() {
      //ChangeVideoDisplayProperty('neutral');
      //PlayVideo('neutral');
    });
    videos.goodChoice.addEventListener('pause', function() {
      //ChangeVideoDisplayProperty('neutral');
      //PlayVideo('neutral');
    });
    videos.badChoice.addEventListener('pause', function() {
    //  ChangeVideoDisplayProperty('neutral');
    //  PlayVideo('neutral');
    });

    videos.goodChoice.addEventListener('play', function() {
    //  videos.neutral.style.display = "none";
    });
    videos.badChoice.addEventListener('play', function() {
      //videos.neutral.style.display = "none";
    });


    startBtn.addEventListener('click', function() {

        if (screenfull.enabled) {
            screenfull.request();
        }

        var isIphone = navigator.userAgent.indexOf('iPad') >= 0;
        if (isIphone) {
            //  showVideoAfterBadChoice();
            //  showVideoAfterGoodChoice();
        };

        showVideoOnRoundStart();
        var player = document.getElementById('back_sound');
        player.src = 'assets/audio/background_sound.mp3';
        player.play();
        beeper = setInterval(function() {
            player.play();
        }, 1000 * 60);
    });

    function ChangeVideoDisplayProperty(videoType) {
        switch (videoType) {
            case "intro":
                videos.intro.style.display = "inline";
                videos.badChoice.style.display = "none";
                videos.goodChoice.style.display = "none";
                moviePlace.style.display="inline";
                loaderArea.style.display="none";
                //videos.neutral.style.display = "none";
                break;
            case "goodChoice":
                videos.intro.style.display = "none";
                videos.badChoice.style.display = "none";
                videos.goodChoice.style.display = "inline";
                //  videos.neutral.style.display = "none";
                break;
            case "badChoice":
                videos.intro.style.display = "none";
                videos.badChoice.style.display = "inline";
                videos.goodChoice.style.display = "none";
                  //videos.neutral.style.display = "none";
                break;
            case "neutral":
                    videos.intro.style.display = "none";
                    videos.badChoice.style.display = "none";
                    videos.goodChoice.style.display = "none";
                    //  videos.neutral.style.display = "initial";
                    break;
        }
    };

    function PlayVideo(videoType) {
        switch (videoType) {
            case "intro":
                videos.goodChoice.muted = true;
                videos.badChoice.muted = true;
                videos.intro.muted = true;
              //  videos.neutral.muted = true;
                //  videos.neutral.currentTime = 0;
                videos.goodChoice.play();
                videos.badChoice.play();
                //videos.neutral.play();
                videos.intro.play();
                break;
            case "goodChoice":
                videos.goodChoice.muted = false;
                videos.intro.pause();
                videos.badChoice.pause();
                //videos.neutral.pause();
                videos.goodChoice.pause();
                videos.intro.currentTime = 0;
                videos.goodChoice.currentTime = 0;
                videos.badChoice.currentTime = 0;
                //videos.neutral.currentTime = 0;
                videos.goodChoice.play();
                break;
            case "badChoice":
                videos.badChoice.muted = false;
                videos.intro.pause();
                //videos.neutral.pause();
                videos.goodChoice.pause();
                videos.badChoice.pause();
                videos.intro.currentTime = 0;
                videos.goodChoice.currentTime = 0;
                videos.badChoice.currentTime = 0;
                //videos.neutral.currentTime = 0;
                videos.badChoice.play();
                break;
                case "neutral":
                    //videos.neutral.muted = false;
                    videos.intro.pause();
                    videos.goodChoice.pause();
                    videos.badChoice.pause();
                    //videos.neutral.pause();
                    videos.intro.currentTime = 0;
                    videos.goodChoice.currentTime = 0;
                    videos.badChoice.currentTime = 0;
                      //videos.neutral.currentTime = 0;
                      //videos.neutral.loop = true;
                    //videos.neutral.play();
                    break;
        }
    }
});
