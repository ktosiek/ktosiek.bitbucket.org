angular.module('app').config(function($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/', {
                templateUrl : 'app/components/rounds/first/view.html',
                controller  : 'roundFirstController'
            })
          });
