$(document).ready(function(){
  $('.sectionSentences__slick').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    nextArrow: '<i class="sectionSentences__slick__arrow"></i>',
    prevArrow: '<i class="sectionSentences__slick__arrow"></i>',
  });
  $('.sectionPhotos__slick').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    nextArrow: '<i class="sectionPhotos__slick__arrow"></i>',
    prevArrow: '<i class="sectionPhotos__slick__arrow"></i>',
    responsive: [
     {
       breakpoint: 768,
       settings: {
         slidesToShow: 1
       }
     }
   ]
  });
  $('.sectionManuals__slick').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: '<i class="sectionManuals__slick__arrow"></i>',
    prevArrow: '<i class="sectionManuals__slick__arrow"></i>',
    responsive: [
      {

        breakpoint: 992,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
   ]
  });


  $('.sectionHeader__showMobileMenu').click(function() {
        $('body').addClass('show-menu');
        return false;
  });
  $('.sectionMobileMenu a').click(function() {
        $('body').removeClass('show-menu');
        return false;
  });
  $('.sectionMobileMenu .sectionMobileMenu__close').click(function() {
        $('body').removeClass('show-menu');
        return false;
  });

  $('.sectionSearch__content').click(function() {
    $(this).addClass('active');
  });

  $('.sectionSearch__closeIcon').on('click', function(e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).parents('.sectionSearch__content').removeClass('active');
  });
});
